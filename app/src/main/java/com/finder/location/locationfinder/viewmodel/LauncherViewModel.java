package com.finder.location.locationfinder.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;

import com.finder.location.locationfinder.data.LocationInfo;
import com.finder.location.locationfinder.interfaces.ILocationManager;
import com.finder.location.locationfinder.model.LocationManagerTask;

/**
 * Class for Data binding with the View
 */
public class LauncherViewModel extends AndroidViewModel implements ILocationManager {

    private final Context mContext;
    public ObservableField<String> mLatitude = new ObservableField<>();
    public ObservableField<String> mLongitude = new ObservableField<>();
    public ObservableInt isVisible = new ObservableInt(8);

    /**
     * Constructor for LauncherViewModel
     *
     * @param application - Application object
     * @param context     - Context object
     */
    public LauncherViewModel(@NonNull Application application, Context context) {
        super(application);
        mContext = context;
    }

    /**
     * Binding method for button press
     */
    public void onGetLocationButtonPressed() {
        LocationManagerTask mLocationTask = new LocationManagerTask(this);
        mLocationTask.execute();
    }

    /**
     * Method to get Context
     *
     * @return - Context object
     */
    public Context getContext() {
        return mContext;
    }

    /**
     * Method to receive Location information
     *
     * @param locationInfo - LocationInfo object
     */
    @Override
    public void onLocationDataReceived(LocationInfo locationInfo) {
        mLatitude.set("Latitude: " + locationInfo.getLatitude());
        mLongitude.set("Longitude: " + locationInfo.getLongitude());
        if (isVisible.get() != 0) {
            isVisible.set(0);
        }
    }
}
