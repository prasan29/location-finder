package com.finder.location.locationfinder.view;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Toast;

import com.finder.location.locationfinder.R;
import com.finder.location.locationfinder.databinding.ActivityLauncherBinding;
import com.finder.location.locationfinder.viewmodel.LauncherViewModel;

import static com.finder.location.locationfinder.model.LocationManagerTask.PERMISSION_REQUEST_CODE;

public class LauncherActivity extends Activity {
    private ActivityLauncherBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    /**
     * Method to initialize Binding and ViewModel
     */
    private void initViewModel() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_launcher);
        LauncherViewModel viewModel = new LauncherViewModel(this.getApplication(), this);
        mBinding.setViewModel(viewModel);
    }

    /**
     * Callback method for Location permission
     *
     * @param requestCode  - Request code for requesting permissions
     * @param permissions  - Permission list
     * @param grantResults - Permission granted or denied
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    mBinding.buttonId.setVisibility(View.GONE);
                    mBinding.errorLocationId.setVisibility(View.VISIBLE);
                    Toast.makeText(this, "Location permission needed! Please enable it in the settings!", Toast.LENGTH_SHORT).show();
                }
        }
    }
}
