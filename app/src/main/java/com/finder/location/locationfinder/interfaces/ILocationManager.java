package com.finder.location.locationfinder.interfaces;

import com.finder.location.locationfinder.data.LocationInfo;

/**
 * Interface for Location information
 */
public interface ILocationManager {

    /**
     * Method to receive Location information
     *
     * @param locationInfo - LocationInfo object
     */
    void onLocationDataReceived(LocationInfo locationInfo);
}
