package com.finder.location.locationfinder.model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;

import com.finder.location.locationfinder.data.LocationInfo;
import com.finder.location.locationfinder.interfaces.ILocationManager;
import com.finder.location.locationfinder.viewmodel.LauncherViewModel;

/**
 * Task to fetch Location information
 */
public class LocationManagerTask extends AsyncTask<Void, Void, LocationInfo> implements LocationListener {
    public static final int PERMISSION_REQUEST_CODE = 1;
    private ILocationManager mListener;
    private LocationManager mLocationManager;

    /**
     * Constructor for LocationManagerTask
     *
     * @param launcherViewModel - LauncherViewModel object
     */
    @SuppressLint("MissingPermission")
    public LocationManagerTask(LauncherViewModel launcherViewModel) {
        mListener = launcherViewModel;
        LauncherViewModel mLauncherViewModel = launcherViewModel;
        mLocationManager = (LocationManager) mLauncherViewModel.getContext().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    protected LocationInfo doInBackground(Void... voids) {
        return fetchLocation();
    }

    /**
     * Method to fetch Location information
     *
     * @return - LocationInfo object
     */
    private LocationInfo fetchLocation() {
        LocationInfo locationInfo = new LocationInfo();
        Criteria criteria = new Criteria();
        String bestProvider = mLocationManager.getBestProvider(criteria, false);
        @SuppressLint("MissingPermission") Location location = mLocationManager.getLastKnownLocation(bestProvider);
        locationInfo.setLatitude("" + location.getLatitude());
        locationInfo.setLongitude("" + location.getLongitude());
        return locationInfo;
    }

    @Override
    protected void onPostExecute(LocationInfo info) {
        super.onPostExecute(info);
        mListener.onLocationDataReceived(info);
    }

    @Override
    public void onLocationChanged(Location location) {
        LocationInfo locationInfo = new LocationInfo();
        locationInfo.setLatitude("" + location.getLatitude());
        locationInfo.setLongitude("" + location.getLongitude());
        mListener.onLocationDataReceived(locationInfo);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
