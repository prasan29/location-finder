package com.finder.location.locationfinder.data;

/**
 * Data class for Location information
 */
public class LocationInfo {
    private String mLatitude;
    private String mLongitude;

    /**
     * Getter for latitude
     *
     * @return - mLatitude - String containing Latitude
     */
    public String getLatitude() {
        return mLatitude;
    }

    /**
     * Setter for Latitude
     *
     * @param mLatitude - String containing Latitude
     */
    public void setLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    /**
     * Getter for Longitude
     *
     * @return - mLongitude - String containing Longitude
     */
    public String getLongitude() {
        return mLongitude;
    }

    /**
     * Setter for Longitude
     *
     * @param mLongitude - String containing Longitude
     */
    public void setLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }
}
